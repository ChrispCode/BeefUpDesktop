// @flow
import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import {restaurantGridReducer} from '../containers/RestaurantGrid/reducer';
import {restaurantAddPopupReducer} from '../containers/RestaurantPopups/Add/reducer';
import {staffAddPopupReducer} from '../containers/StaffPopups/Add/reducer';
import {restaurantEditPopupReducer} from '../containers/RestaurantPopups/Edit/reducer'

const rootReducer = combineReducers({
  restaurantEditPopupReducer,
  staffAddPopupReducer,
  restaurantAddPopupReducer,
  restaurantGridReducer,
  routing
});

export default rootReducer;
