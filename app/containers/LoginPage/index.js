import React, { Component, PropTypes } from 'react';
import { Input, Button, Header, Message } from 'semantic-ui-react';

import { graphql, compose } from 'react-apollo';
import { withRouter } from 'react-router';
import gql from 'graphql-tag';

import style from './style.css';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
    this.state = {
      username: '',
      password: '',
      error: '',
    };
  }

  componentWillReceiveProps() {
    if (localStorage.getItem('token')) {
      this.props.router.push('/restaurants');
    }
  }

  handleInputChange(input) {
    return ({ target }) => {
      this.setState({ [input]: target.value });
    };
  }

  handleLoginSubmit() {
    const { username, password } = this.state;
    this.props
      .createToken({ variables: { username, password } })
      .then(({ data }) => {
        localStorage.setItem('token', data.createToken.token);
        return this.props.router.push('/');
      })
      .catch(err => {
        this.setState({ error: err.graphQLErrors[0].message });
      });
  }

  render() {
    return (
      <div className={style.wrap}>
        <Header as="h1" className={style.header}>BeefUp</Header>
        <Input placeholder="Username" onChange={this.handleInputChange('username')} />
        <Input
          placeholder="Password"
          type="password"
          onChange={this.handleInputChange('password')}
        />
        <Button primary onClick={this.handleLoginSubmit}>Login</Button>
        {!!this.state.error && <Message color="red">{this.state.error}</Message>}
      </div>
    );
  }
}

Login.propTypes = {
  createToken: PropTypes.func.isRequired,
  router: PropTypes.objectOf({
    push: PropTypes.func
  }).isRequired
};

const createToken = gql`
  mutation ($username: String!, $password: String!) {
    createToken(username: $username, password: $password) {
      token
    }
  }
`;

export default compose(
  graphql(createToken, { name: 'createToken' })
)(withRouter(Login));
