import * as actionTypes from './actionTypes'

export function checkRestaurantGridRow (id) {
  return {
    type    : actionTypes.CHECK_RESTAURANT_GRID_ROW,
    payload : { id }
  }
}

export function uncheckRestaurantGridRow (id) {
  return {
    type  : actionTypes.UNCHECK_RESTAURANT_GRID_ROW,
    payload: { id }
  }
}

export function resetRestaurantGridRow () {
  return {
    type  : actionTypes.RESET_RESTAURANT_GRID_ROW
  }
}
