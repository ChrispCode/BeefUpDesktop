import * as actionTypes from './actionTypes'
import { Map, List } from 'immutable'

const initialState = Map({
  checkedRows: List(),
})

const ACTION_HANDLERS = {
  [actionTypes.CHECK_RESTAURANT_GRID_ROW]  : (state, action) => {
    return state.update('checkedRows', (v) => v.push(action.payload.id))
  },
  [actionTypes.UNCHECK_RESTAURANT_GRID_ROW]  : (state, action) => {
    return state.update('checkedRows', (v) => v.filter((item) => item !== action.payload.id))
  },
  [actionTypes.RESET_RESTAURANT_GRID_ROW]  : (state, action) => {
    return state.set('checkedRows', initialState.get('checkedRows'))
  }
}

export function restaurantGridReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
