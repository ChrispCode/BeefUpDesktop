import gql from 'graphql-tag'

export const getAllRestaurants = gql`
    query {
        restaurants {
           id
           name
           country
           city
           street_name
           street_number
           balance
           clients {
            id
           }
           staff {
            id
           }
           halls {
            id
           }
        }
    }
`
