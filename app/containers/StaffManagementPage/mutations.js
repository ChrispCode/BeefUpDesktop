import gql from 'graphql-tag'

export const createRestaurant = gql`
    mutation ($name: String!, $address: String!) {
        createRestaurant(name: $name, address: $address) {
            id
        }
    }
`

export const editRestaurant = gql`
    mutation ($id: Int!, $name: String!, $address: String!) {
        updateRestaurant(id: $id, name: $name, address: $address) {
            id
        }
    }
`

export const removeRestaurant = gql`
    mutation ($id: Int!) {
        removeRestaurant(id: $id) {
            id
        }
    }
`
