import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Icon, Dimmer, Loader, Menu, Card } from 'semantic-ui-react'

import AddPopup from '../StaffPopups/Add/'
import Header from '../Header'

import { bindActionCreators } from 'redux'
import { togglePopup } from '../RestaurantPopups/Add/actions'

import { graphql, compose } from 'react-apollo'
import { withRouter } from 'react-router'
import gql from 'graphql-tag'

import style from './style.css'

export class StaffManagementPage extends Component {

  constructor (props) {
    super(props)
    if (!localStorage.getItem('token')) {
      this.props.router.push('/')
    }
  }

  render () {
    const { togglePopup } = this.props
    return (
      <div>
        {this.props.data.loading && <Dimmer active inverted><Loader inverted content='Loading' /></Dimmer>}
        <Header active='staff' />
        <div className={style.wrap}>
          <Menu size='small'>
            <Menu.Item name='Restaurant Management - Actions' />
            <Menu.Menu position='right'>
              <Menu.Item>
                <Button animated='vertical' positive floated='right' onClick={togglePopup} >
                  <Button.Content visible>
                    Add
                  </Button.Content>
                  <Button.Content hidden>
                    <Icon name='plus' />
                  </Button.Content>
                </Button>
              </Menu.Item>
            </Menu.Menu>
          </Menu>
          <Card.Group>
          {this.props.data.restaurants && this.props.data.restaurants.map((restaurant) => {
            return restaurant.staff && restaurant.staff.map((staffMember) => {
              return (
                <Card fluid color='blue'>
                <Card.Content>
                  <Card.Header>{`${staffMember.first_name}  ${staffMember.last_name}`}</Card.Header>
                  <Card.Meta>{staffMember.role}</Card.Meta>
                  {/*<div className='ui two buttons' style={{float: 'right', float: 'right', position: 'absolute', top: '15px', right: '20px', width: 'initial'}}>
                    <Button basic color='blue'>Edit</Button>
                  </div>*/}
                </Card.Content>
              </Card>
              );
            })
          })}
          </Card.Group>


          <AddPopup refetch={this.props.data.refetch} restaurants={this.props.data.restaurantIdNames}/>
        </div>
      </div>
    )
  }

}

StaffManagementPage.propTypes = {
  data: React.PropTypes.shape({
    loading: React.PropTypes.bool,
    error: React.PropTypes.object,
    restaurants: React.PropTypes.array,
    refetch: React.PropTypes.func
  }).isRequired,
  router: React.PropTypes.object.isRequired,
  togglePopup: React.PropTypes.func.isRequired
}

const getStaff = gql`
  query {
    restaurants {
      staff {
        id
        username
        role
        first_name
        last_name
      }
    }
  	restaurantIdNames: restaurants {
      value: id
      key: name
      text: name
    }
  }
`

const mapStateToProps = state => ({
  // restaurantModal: state.restaurants.restaurantModal
})

const mapDispatchToProps = (dispatch) => ({
  togglePopup: bindActionCreators(togglePopup, dispatch)
})

export default compose(
  graphql(getStaff),
  connect(mapStateToProps, mapDispatchToProps)
)(withRouter(StaffManagementPage))
