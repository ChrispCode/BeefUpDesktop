import * as actionTypes from './actionTypes'

export function setLoadingRestaurantModal (isLoading = true) {
  return {
    type    : actionTypes.SET_LOADING_RESTAURANT_MODAL,
    payload : {
      loading: isLoading
    }
  }
}

export function setSuccessRestaurantModal () {
  return {
    type    : actionTypes.SET_SUCCESS_RESTAURANT_MODAL,
    payload : {}
  }
}

export function setFailRestaurantModal (error) {
  return {
    type    : actionTypes.SET_FAIL_RESTAURANT_MODAL,
    payload : {
      hasError: true,
      errorMessage: error.message
    }
  }
}
