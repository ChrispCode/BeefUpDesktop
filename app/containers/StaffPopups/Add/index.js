import React, { PropTypes } from 'react'
import { Form, Modal, Button, Loader, Dimmer, Dropdown, Input, Message } from 'semantic-ui-react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { bindActionCreators } from 'redux'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import style from './style.css'

class StaffAddPopup extends React.Component {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleCreate = this.handleCreate.bind(this)
    this.state = { isLoading: false, error: '' }
  }

  handleInputChange (input) {
    return (event, data) => {
      this.props.actions.changeInputValue(input, data.value)
    }
  }

  handleCreate () {
    let { username, password, email, first_name, last_name, role, restaurant_id } = this.props
    if(username && password && email && first_name && last_name && role && restaurant_id) {
        this.setState({ isLoading: true })
      this.props.createUser({ variables: { username, password, email, first_name, last_name, role, restaurant_id: parseInt(restaurant_id) } }).then(() => {
        this.props.refetch()
        this.props.actions.togglePopup()
        this.setState({ isLoading: false })
      }).catch((err) => {
        console.log(err);
        this.setState({ error: err.graphQLErrors[0].message, isLoading: false })
      })
    } else {
      this.setState({error : 'All fields should be populated'})
    }

  }

  render () {
    return (
      <Modal size='small' open={this.props.isOpen} onClose={() => {this.setState({error: ''}); this.props.actions.togglePopup()}}>
        <Dimmer active={this.state.isLoading} inverted><Loader>Loading</Loader></Dimmer>
        <Modal.Header>
          Add New Employee
        </Modal.Header>
        <Modal.Content className={style.formCenter}>
          {!!this.state.error && <Message color='red'>{this.state.error}</Message>}
          <Form >
            <Form.Field required>
              <label>Username</label>
              <Input placeholder='Username' onChange={this.handleInputChange('username')} />
            </Form.Field>
            <Form.Field required>
              <label>Password</label>
              <Input placeholder='Password' onChange={this.handleInputChange('password')} />
            </Form.Field>
             <Form.Field required>
              <label>E-mail</label>
              <Input placeholder='E-mail' onChange={this.handleInputChange('email')} />
            </Form.Field>
            <Form.Field required>
              <label>First Name</label>
              <Input placeholder='First Name' onChange={this.handleInputChange('first_name')} />
            </Form.Field>
            <Form.Field required>
              <label>Last Name</label>
              <Input placeholder='Last Name' onChange={this.handleInputChange('last_name')} />
            </Form.Field>
            <Form.Field required>
              <label>Restaurant</label>
              <Dropdown placeholder='Restaurant' search selection options={this.props.restaurants || []} onChange={this.handleInputChange('restaurant_id')} />
            </Form.Field>
            <Form.Field required>
              <label>Role</label>
              <Dropdown placeholder='Role' search selection options={[
                {key: 'Cook', text: 'Cook', value: 'cook'},
                {key: 'Barman', text: 'Barman', value: 'barman'},
                {key: 'Cashier', text: 'Cashier', value: 'cashier'},
                {key: 'Waiter', text: 'Waiter', value: 'waiter'}
              ]} onChange={this.handleInputChange('role')}/>
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button type='submit' color='blue' onClick={this.handleCreate}>Create</Button>
          <Button floated='right' onClick={() => {this.setState({error: ''}); this.props.actions.togglePopup()}}>Cancel</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

StaffAddPopup.propTypes = {
  name: PropTypes.string,
  address: PropTypes.string,
  isOpen: PropTypes.bool,
  actions: PropTypes.object,
  createRestaurant: PropTypes.func,
  refetch: PropTypes.func,
  restaurants: PropTypes.array
}

const createUser = gql`
  mutation ($username: String!, $password: String!, $email: String!, $first_name: String!, $last_name: String!, $restaurant_id: Int!, $role: UserRole) {
    createUser(username: $username, password: $password, email: $email, first_name: $first_name, last_name: $last_name, restaurant_id: $restaurant_id, role: $role) {
        id
    }
  }
`

const mapStateToProps = state => ({
  username: state.restaurantAddPopupReducer.get('username'),
  password: state.restaurantAddPopupReducer.get('password'),
  email: state.restaurantAddPopupReducer.get('email'),
  first_name: state.restaurantAddPopupReducer.get('first_name'),
  last_name: state.restaurantAddPopupReducer.get('last_name'),
  role: state.restaurantAddPopupReducer.get('role'),
  restaurant_id: state.restaurantAddPopupReducer.get('restaurant_id'),
  isOpen: state.restaurantAddPopupReducer.get('isOpen')
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
})

export default compose(
  graphql(createUser, { name: 'createUser' }),
  connect(mapStateToProps, mapDispatchToProps)
)(StaffAddPopup)
