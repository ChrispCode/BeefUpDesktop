import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react';
import { Link } from 'react-router';
import { withRouter } from 'react-router';

class Header extends Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    localStorage.removeItem('token');
    this.props.router.push('/');
  }

  render() {
    return (
      <div>
        <Menu size="large" color="blue">
          <Menu.Item name="BeefUp Administration" header />
          <Menu.Item
            name="Restaurant Management"
            active={this.props.active === 'restaurant'}
            onClick={() => this.props.router.push('/restaurants')}
          />
          <Menu.Item
            name="Staff Management"
            active={this.props.active === 'staff'}
            onClick={() => this.props.router.push('/staff')}
          />
          {/* <Menu.Item name='Hall and Table Management' active={this.props.active === 'Hall'}/>
          <Menu.Item name='Kitchen' active={this.props.active === 'Kitchen'}/>*/
          }
          <Menu.Menu position="right">
            <Menu.Item name="help" onClick={this.handleLogout}>
              Logout
            </Menu.Item>
          </Menu.Menu>
        </Menu>
      </div>
    );
  }
}

export default withRouter(Header);
