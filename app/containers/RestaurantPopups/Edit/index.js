import React, { PropTypes } from 'react'
import { Form, Modal, Button, Loader, Dimmer, Message } from 'semantic-ui-react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { bindActionCreators } from 'redux'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import style from './style.css'

class GridEditPopup extends React.Component {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.state = { isLoading: false, error: '' }
  }

  componentWillReceiveProps(props) {
    this.setState({name: props.data.restaurant.name, address: props.data.restaurant.city + " " + props.data.restaurant.street_name})
  }

  handleInputChange (input) {
    return ({ target }) => {
      this.setState({input: target.value})
    }
  }

  handleUpdate () {
    let { id, name, address } = this.props
    if(name && address) {
      this.setState({ isLoading: true })
      this.props.updateRestaurant({ variables: { id, name, address } }).then(() => {
        this.props.refetch()
        this.props.actions.togglePopup()
        this.setState({ isLoading: false })
      }).catch((err) => {
        this.setState({ error: err.graphQLErrors[0].message, isLoading: false })
      })
    } else {
      this.setState({error : 'All fields should be populated'})
    }
  }

  render () {
    return (
      <Modal size='small' open={this.props.isOpen} onClose={this.props.actions.togglePopup}>
        <Dimmer active={this.state.isLoading} inverted><Loader>Loading</Loader></Dimmer>
        <Modal.Header>
          Edit Restaurant
        </Modal.Header>
        <Modal.Content className={style.formCenter}>
          {!!this.state.error && <Message color='red'>{this.state.error}</Message>}
          <Form>
            <Form.Field required>
              <label>Restaurant Name</label>
              <input placeholder='Restaurant Name' onChange={this.handleInputChange('name')} value={this.state.name}/>
            </Form.Field>
            <Form.Field required>
              <label>Restaurant Address</label>
              <input placeholder='Restaurant Address' onChange={this.handleInputChange('address')} value={this.state.address}/>
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button color='blue' onClick={this.handleUpdate}>Create</Button>
          <Button floated='right' onClick={this.props.actions.togglePopup}>Cancel</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

GridEditPopup.propTypes = {
  data: PropTypes.object,
  name: PropTypes.string,
  address: PropTypes.string,
  isOpen: PropTypes.bool,
  actions: PropTypes.object,
  createRestaurant: PropTypes.func,
  refetch: PropTypes.func
}

const updateRestaurant = gql`
  mutation ($id: Int!, $name: String!, $address: String!) {
    updateRestaurant(id: $id, name: $name, address: $address) {
        id
    }
  }
`

export const getCurrentRestaurant = gql`
  query getCurrentRestaurant($id: Int!) {
    restaurant(id: $id) {
      name
      city
      country
      street_name
    }
  }
`


const mapStateToProps = state => ({
  id: state.restaurantGridReducer.get(0),
  isOpen: state.restaurantEditPopupReducer.get('isOpen')
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
})

export default compose(
  graphql(getCurrentRestaurant, {options: (ownProps) => ({variables: {id: ownProps.id}})}),
  graphql(updateRestaurant, { name: 'updateRestaurant' }),
  connect(mapStateToProps, mapDispatchToProps)
)(GridEditPopup)
