import * as actionTypes from './actionTypes'
import { Map } from 'immutable'

const ACTION_HANDLERS = {
  [actionTypes.CHANGE_ADD_RESTAURANT_INPUT_VALUE]  : (state, action) => {
    return state.set(action.payload.input, action.payload.value)
  },
  [actionTypes.TOGGLE_ADD_RESTAURANT_POPUP]  : (state, action) => {
    return state.update('isOpen', (v) => !v)
  }
}

const initialState = Map({
  isOpen: false,
  name: '',
  address: ''
})

export function restaurantEditPopupReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
