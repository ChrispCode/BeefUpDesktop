import * as actionTypes from './actionTypes'

export function changeInputValue (input, value) {
  return {
    type    : actionTypes.CHANGE_ADD_RESTAURANT_INPUT_VALUE,
    payload : { input, value }
  }
}

export function togglePopup () {
  return {
    type  : actionTypes.TOGGLE_ADD_RESTAURANT_POPUP
  }
}

