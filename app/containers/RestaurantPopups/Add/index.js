import React, { PropTypes } from 'react'
import { Form, Modal, Button, Loader, Dimmer, Message } from 'semantic-ui-react'
import { connect } from 'react-redux'
import * as actions from './actions'
import { bindActionCreators } from 'redux'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import style from './style.css'

class GridAddPopup extends React.Component {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleCreate = this.handleCreate.bind(this)
    this.state = { isLoading: false, error: '' }
  }

  handleInputChange (input) {
    return ({ target }) => {
      this.props.actions.changeInputValue(input, target.value)
    }
  }

  handleCreate () {
    let { name, address } = this.props
    if(name && address) {
      this.setState({ isLoading: true })
      this.props.createRestaurant({ variables: { name, address } }).then(() => {
        this.props.refetch()
        this.props.actions.togglePopup()
        this.setState({ isLoading: false })
      }).catch((err) => {
        this.setState({ error: err.graphQLErrors[0].message, isLoading: false })
      })
    } else {
      this.setState({error : 'All fields should be populated'})
    }
  }

  render () {
    return (
      <Modal size='small' open={this.props.isOpen} onClose={this.props.actions.togglePopup}>
        <Dimmer active={this.state.isLoading} inverted><Loader>Loading</Loader></Dimmer>
        <Modal.Header>
          Add New Restaurant
        </Modal.Header>
        <Modal.Content className={style.formCenter}>
          {!!this.state.error && <Message color='red'>{this.state.error}</Message>}
          <Form>
            <Form.Field required>
              <label>Restaurant Name</label>
              <input placeholder='Restaurant Name' onChange={this.handleInputChange('name')} />
            </Form.Field>
            <Form.Field required>
              <label>Restaurant Address</label>
              <input placeholder='Restaurant Address' onChange={this.handleInputChange('address')} />
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button color='blue' onClick={this.handleCreate}>Create</Button>
          <Button floated='right' onClick={this.props.actions.togglePopup}>Cancel</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

GridAddPopup.propTypes = {
  name: PropTypes.string,
  address: PropTypes.string,
  isOpen: PropTypes.bool,
  actions: PropTypes.object,
  createRestaurant: PropTypes.func,
  refetch: PropTypes.func
}

const createRestaurant = gql`
  mutation ($name: String!, $address: String!) {
    createRestaurant(name: $name, address: $address) {
        id
    }
  }
`

const mapStateToProps = state => ({
  name: state.restaurantAddPopupReducer.get('name'),
  address: state.restaurantAddPopupReducer.get('address'),
  isOpen: state.restaurantAddPopupReducer.get('isOpen')
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
})

export default compose(
  graphql(createRestaurant, { name: 'createRestaurant' }),
  connect(mapStateToProps, mapDispatchToProps)
)(GridAddPopup)
