import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Icon, Dimmer, Loader, Menu } from 'semantic-ui-react'

import AddPopup from '../RestaurantPopups/Add'
// import EditPopup from '../RestaurantPopups/Edit'

import Grid from '../RestaurantGrid'
import Header from '../Header'

import { bindActionCreators } from 'redux'
import { togglePopup as toggleAddPopup } from '../RestaurantPopups/Add/actions'
import { togglePopup as toggleEditPopup } from '../RestaurantPopups/Edit/actions'
import { resetRestaurantGridRow } from '../RestaurantGrid/actions'
import {removeRestaurant} from './mutations'

import { graphql, compose } from 'react-apollo'
import { withRouter } from 'react-router'
import gql from 'graphql-tag'

import style from './style.css'

export class Restaurants extends Component {

  constructor (props) {
    super(props)
    this.handleDelete = this.handleDelete.bind(this)
  }

   handleDelete() {
    this.props.checkedRows.map((id) => {
      this.props.removeRestaurant({ variables: { id } }).then(() => {
        this.props.data.refetch()
        this.props.resetRestaurantGridRow()
      })
    })
  }

  componentWillReceiveProps() {
    if (!localStorage.getItem('token')) {
      this.props.router.push('/')
    }
  }

  render () {
    const { toggleEditPopup, toggleAddPopup } = this.props
    return (
      <div>
        {this.props.data.loading && <Dimmer active inverted><Loader inverted content='Loading' /></Dimmer>}
        <Header active='restaurant' router={this.props.router}/>
        <div className={style.wrap}>
          <Menu size='small'>
            <Menu.Item name='Restaurant Management - Actions' />
            <Menu.Menu position='right'>
              <Menu.Item>
                <Button floated='right' color='red' size='small' onClick={this.handleDelete} disabled={this.props.checkedRows.length === 0}>
                  Delete Selected
                </Button>
              </Menu.Item>
              {/*<Menu.Item>
                <Button floated='right' color='blue' size='small' onClick={toggleEditPopup} disabled={this.props.checkedRows.length !== 1}>
                  Edit
                </Button>
              </Menu.Item>*/}
               <Menu.Item>
                <Button animated='vertical' positive floated='right' onClick={toggleAddPopup} >
                  <Button.Content visible>
                    Add
                  </Button.Content>
                  <Button.Content hidden>
                    <Icon name='plus' />
                  </Button.Content>
                </Button>
            </Menu.Item>
            </Menu.Menu>

          </Menu>
          <Grid data={this.props.data} refetch={this.props.data.refetch}/>
          <AddPopup refetch={this.props.data.refetch} />
          {/*<EditPopup refetch={this.props.data.refetch} />*/}
        </div>
      </div>
    )
  }

}

Restaurants.propTypes = {
  data: React.PropTypes.shape({
    loading: React.PropTypes.bool,
    error: React.PropTypes.object,
    restaurants: React.PropTypes.array,
    refetch: React.PropTypes.func
  }).isRequired,
  router: React.PropTypes.object.isRequired,
  togglePopup: React.PropTypes.func.isRequired
}

const getAllRestaurants = gql`
  query {
    restaurants {
      id
      name
      country
      city
      street_name
      street_number
      balance
      clients {
        id
      }
      staff {
        id
      }
      halls {
        id
      }
    }
  }
`

const mapStateToProps = state => ({
  checkedRows: state.restaurantGridReducer.get('checkedRows').toJS() || []
})

const mapDispatchToProps = (dispatch) => ({
  toggleAddPopup: bindActionCreators(toggleAddPopup, dispatch),
  toggleEditPopup: bindActionCreators(toggleEditPopup, dispatch),
  resetRestaurantGridRow: bindActionCreators(resetRestaurantGridRow, dispatch)
})

export default compose(
  graphql(getAllRestaurants),
  graphql(removeRestaurant, {name: 'removeRestaurant'}),
  connect(mapStateToProps, mapDispatchToProps)
)(withRouter(Restaurants))
