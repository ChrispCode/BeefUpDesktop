import React from 'react';
import { render } from 'react-dom';
// import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import routes from './routes';
import configureStore from './store/configureStore';
import './app.global.css';

import { ApolloProvider } from 'react-apollo';
import ApolloClient, { createNetworkInterface, addTypeName } from 'apollo-client';

const store = configureStore();
const history = syncHistoryWithStore(hashHistory, store);

const networkInterface = createNetworkInterface({ uri: 'https://graphql.beefup.eu/' });

networkInterface.use([
  {
    applyMiddleware(req, next) {
      if (!req.options.headers) {
        req.options.headers = {};
      }

      const token = localStorage.getItem('token');
      if (token) {
        req.options.headers.authorization = token;
      }
      next();
    },
  },
]);

export const client = new ApolloClient({
  networkInterface,
  queryTransformer: addTypeName,
});

render(
  <ApolloProvider store={store} client={client}>
    <Router history={history} routes={routes} />
  </ApolloProvider>,
  document.getElementById('root'),
);
