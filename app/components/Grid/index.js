import React, { PropTypes } from 'react';
import { Table, Button, Checkbox, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import * as actions from './actions';
import { bindActionCreators } from 'redux';
import style from './style.css';
import { removeRestaurant } from '../RestaurantPage/mutations';
import { graphql, compose } from 'react-apollo';

class GridAddPopup extends React.Component {
  constructor(props) {
    super(props);
    this.handleCheckboxClick = this.handleCheckboxClick.bind(this);
  }

  handleCheckboxClick(restaurant) {
    return (event, data) => {
      data.checked
        ? this.props.actions.uncheckRestaurantGridRow(restaurant)
        : this.props.actions.checkRestaurantGridRow(restaurant);
    };
  }

  render() {
    return (
      <Table striped color="blue">
        <Table.Header fullWidth>
          <Table.Row>
            <Table.HeaderCell />
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Country</Table.HeaderCell>
            <Table.HeaderCell>City</Table.HeaderCell>
            <Table.HeaderCell>Street Name</Table.HeaderCell>
            <Table.HeaderCell>Current Balance</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        {this.props.data.restaurants &&
          this.props.data.restaurants.map(restaurant => (
            <Table.Body key={restaurant.id}>
              <Table.Row active={this.props.checkedRows.includes(restaurant.id)}>
                <Table.Cell collapsing>
                  <Checkbox onClick={this.handleCheckboxClick(restaurant)} />
                </Table.Cell>
                <Table.Cell>{restaurant.name}</Table.Cell>
                <Table.Cell>{restaurant.country}</Table.Cell>
                <Table.Cell>{restaurant.city}</Table.Cell>
                <Table.Cell>{`${restaurant.street_name} ${restaurant.street_number}`}</Table.Cell>
                <Table.Cell>{restaurant.balance}</Table.Cell>
              </Table.Row>
            </Table.Body>
            ))}
      </Table>
    );
  }
}

GridAddPopup.propTypes = {
  data: PropTypes.object,
  refetch: PropTypes.func,
};

const mapStateToProps = state => ({
  name: state.restaurantAddPopupReducer.get('name'),
  address: state.restaurantAddPopupReducer.get('address'),
  isOpen: state.restaurantAddPopupReducer.get('isOpen'),
  checkedRows: state.restaurantGridReducer.get('checkedRows').toJS() || [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(GridAddPopup);
