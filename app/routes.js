// @flow
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import Login from './containers/LoginPage'
import Restaurant from './containers/RestaurantPage'
import Staff from './containers/StaffManagementPage'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Login} />
    <Route path="/restaurants" component={Restaurant} />
    <Route path="/staff" component={Staff} />
  </Route>
);
